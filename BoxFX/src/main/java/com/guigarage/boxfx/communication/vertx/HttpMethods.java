package com.guigarage.boxfx.communication.vertx;

public enum HttpMethods {
	PUT("Put"), POST("Post");
	
	private String name;
	
	private HttpMethods(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
