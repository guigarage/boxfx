package com.guigarage.boxfx.application;

import javafx.scene.image.Image;

import com.guigarage.boxfx.application.task.ApplicationBackgroundTaskFactory;

public class BoxFXApplicationMetadata implements ApplicationMetadata {

	private Image snapshot;

	private String applicationClass;

	private String backgroundTaskFactoryClass;

	private String name;

	private String version;

	private String vendor;

	private String artifactId;

	private Image icon;

	private ClassLoader applicationClassLoader;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVendor() {
		return vendor;
	}

	public String getVersion() {
		return version;
	}

	public String getApplicationClass() {
		return applicationClass;
	}

	public void setApplicationClass(String applicationClass) {
		this.applicationClass = applicationClass;
	}

	public String getBackgroundTaskFactoryClass() {
		return backgroundTaskFactoryClass;
	}

	public void setBackgroundTaskFactoryClass(String backgroundTaskFactoryClass) {
		this.backgroundTaskFactoryClass = backgroundTaskFactoryClass;
	}

	public Image getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(Image snapshot) {
		this.snapshot = snapshot;
	}

	public Image getIcon() {
		return icon;
	}

	public void setIcon(Image icon) {
		this.icon = icon;
	}

	public String getArtifactId() {
		return artifactId;
	}

	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public ClassLoader getApplicationClassLoader() {
		return applicationClassLoader;
	}

	public void setApplicationClassLoader(ClassLoader applicationClassLoader) {
		this.applicationClassLoader = applicationClassLoader;
	}

	@SuppressWarnings("unchecked")
	public synchronized <T extends Application> T loadApplication()
			throws ApplicationException {
		try {
			Class<T> cls = (Class<T>) applicationClassLoader
					.loadClass(applicationClass);
			return (T) cls.newInstance();
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public synchronized ApplicationBackgroundTaskFactory loadBackgroundTaskFactory()
			throws ApplicationException {
		try {
			if (backgroundTaskFactoryClass == null) {
				return null;
			}
			Class<? extends ApplicationBackgroundTaskFactory> cls = (Class<? extends ApplicationBackgroundTaskFactory>) applicationClassLoader
					.loadClass(backgroundTaskFactoryClass);
			return cls.getConstructor().newInstance();
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}

	public String getUniqueName() {
		return getUniqueAppName(vendor, artifactId, version);
	}

	public static String getUniqueAppName(String vendor, String name,
			String version) {
		return vendor + "-" + name + "-" + version;
	}
}
