package com.guigarage.boxfx.view;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import org.datafx.controller.ViewFactory;
import org.datafx.controller.context.ViewContext;
import org.datafx.controller.util.FxmlLoadException;

import com.guigarage.boxfx.application.Application;
import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.notification.ApplicationNotificationHandler;
import com.guigarage.boxfx.notification.Notification;
import com.guigarage.boxfx.notification.NotificationHandler;
import com.guigarage.boxfx.runtime.ApplicationRuntime;
import com.guigarage.boxfx.view.skin.BlurWrapperPane;

public class BoxView {

	private Pane mainPane;

	private Pane dialogLayerPane;

	private Pane applicationView;

	private ImageView wallpaperView;

	private Pane notificationPane;

	private Pane homeMenupane;

	private MainMenuController mainMenuController;

	private Application currentApplication;

	public BoxView() {
		mainPane = new StackPane();
		mainPane.setFocusTraversable(false);
		try {
			mainPane.getChildren().add(ViewFactory.getInstance().createByController(SplashScreenController.class).getRootNode());
		} catch (FxmlLoadException e) {
			e.printStackTrace();
		}
	}

	public Node getHomeMenupane() {
		if (homeMenupane == null) {
			try {
				ViewContext<MainMenuController> context = ViewFactory
						.getInstance().createByController(
								MainMenuController.class);
				mainMenuController = context.getController();
				homeMenupane = (Pane) context.getRootNode();
				homeMenupane.setFocusTraversable(false);
			} catch (FxmlLoadException e) {
				e.printStackTrace();
			}
		}
		return homeMenupane;
	}

	public Pane getApplicationView() {
		if (applicationView == null) {
			applicationView = new StackPane();
			applicationView.setFocusTraversable(false);
			applicationView.setStyle("-fx-background-color: #000000;");
			applicationView.setVisible(false);
		}
		return applicationView;
	}

	public Pane getDialogLayerPane() {
		if (dialogLayerPane == null) {
			dialogLayerPane = new BlurWrapperPane();
			dialogLayerPane.setFocusTraversable(false);
			dialogLayerPane.setVisible(false);
		}
		return dialogLayerPane;
	}

	public ImageView getWallpaperView() {
		if (wallpaperView == null) {
//			wallpaperView = new ImageView(Settings.getInstance()
//					.wallpaperProperty().getValue().getUrl().toString());
			wallpaperView.setFocusTraversable(false);
//			Settings.getInstance().wallpaperProperty()
//					.addListener(new ChangeListener<Wallpaper>() {
//
//						@Override
//						public void changed(
//								ObservableValue<? extends Wallpaper> observable,
//								Wallpaper oldValue, Wallpaper newValue) {
//							wallpaperView.setImage(new Image(newValue.getUrl()
//									.toString()));
//						}
//					});
		}
		return wallpaperView;
	}

	public Pane getNotificationPane() {
		if (notificationPane == null) {
			notificationPane = new StackPane();
			notificationPane.setFocusTraversable(false);
			notificationPane.setVisible(false);
		}
		return notificationPane;
	}

	public void addToMenu(final ApplicationMetadata applicationMetadata) {
		//TODO: Hack to create the Controller
		getHomeMenupane();
		
		mainMenuController.addToMenu(applicationMetadata);
	}

	public void removeFromMenu(ApplicationMetadata applicationMetadata) {
		//TODO: Hack to create the Controller
		getHomeMenupane();
		
		mainMenuController.removeFromMenu(applicationMetadata);
	}

	public void removeCurrentApplication(Image applicationSnapshot) {
		currentApplication = null;
		getApplicationView().getChildren().clear();

		getHomeMenupane().setVisible(true);
		getWallpaperView().setVisible(true);
		getApplicationView().setVisible(false);
	}

	public void lockUI(String uid) {
	}

	public void unlockUI(String uid) {
	}

	public void waitTillAnimationsFinished() {
	}

	public ApplicationViewWrapper showApplication(Image snapshot) {

		return new ApplicationViewWrapper() {

			@Override
			public ApplicationNotificationHandler getNotificationHandler() {
				return BoxView.this.getNotificationHandler();
			}

			@Override
			public void setApplication(Application application) {
				currentApplication = application;
				getApplicationView().getChildren().clear();
				getApplicationView().getChildren().add(application.show());
				getHomeMenupane().setVisible(false);
				getApplicationView().setVisible(true);
			}
		};
	}

	public ApplicationNotificationHandler getNotificationHandler() {
		return new ApplicationNotificationHandler() {

			@Override
			public void show(ApplicationMetadata metadata, Notification notification) {
				// notificationPane.setText(notification.getText());
				// notificationPane.show();
			}
		};
	}

	public void show(ApplicationRuntime applicationRuntime) {
		mainPane.getChildren().clear();
		mainPane.getChildren().add(getWallpaperView());
		mainPane.getChildren().add(getHomeMenupane());
		mainPane.getChildren().add(getApplicationView());
		mainPane.getChildren().add(getNotificationPane());
		mainPane.getChildren().add(getDialogLayerPane());
		
		mainMenuController.setApplicationRuntime(applicationRuntime);
	}

	public Parent getView() {
		return mainPane;
	}

}
