package com.guigarage.boxfx.view;

import com.guigarage.boxfx.application.Application;
import com.guigarage.boxfx.notification.ApplicationNotificationHandler;

public interface ApplicationViewWrapper {

	ApplicationNotificationHandler getNotificationHandler();

	void setApplication(Application application);
}
