package com.guigarage.boxfx.concurrency;

import java.util.concurrent.Callable;

import javafx.scene.Node;

import com.guigarage.boxfx.application.Application;
import com.guigarage.boxfx.application.ApplicationMetadata;

public class ApplicationConcurrencyWrapper implements Application {

	private Application application;
	
	private BoxApplicationExecutor executor;
	
	public ApplicationConcurrencyWrapper(Application application, BoxApplicationExecutor executor) {
		this.application = application;
		this.executor = executor;
	}

	@Override
	public ApplicationMetadata getMetadata() {
		try {
			return executor.submit(new Callable<ApplicationMetadata>() {

				@Override
				public ApplicationMetadata call() throws Exception {
					return application.getMetadata();
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void prepairStop() {
		try {
			executor.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					application.prepairStop();
					return null;
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void start(final ApplicationMetadata applicationMetadata) {
		try {
			executor.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					application.start(applicationMetadata);
					return null;
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void showMenu() {
		try {
			executor.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					application.showMenu();
					return null;
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void pause() {
		try {
			executor.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					application.pause();
					return null;
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}	
	}

	@Override
	public void resume() {
		try {
			executor.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					application.resume();
					return null;
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}

	@Override
	public Node show() {
		try {
			return executor.submit(new Callable<Node>() {

				@Override
				public Node call() throws Exception {
					return application.show();
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}		
	}

	@Override
	public void end() {
		try {
			executor.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					application.end();
					return null;
				}
			}).get();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}	
	}
}
