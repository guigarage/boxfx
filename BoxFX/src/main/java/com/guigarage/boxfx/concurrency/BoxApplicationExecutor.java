package com.guigarage.boxfx.concurrency;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class BoxApplicationExecutor implements ApplicationExecutor{

	private ExecutorService applicationExecutorService;
	
	private ThreadFactory applicationThreadFactory;
	
	public BoxApplicationExecutor(ExecutorService applicationExecutorService, ThreadFactory applicationThreadFactory) {
		this.applicationThreadFactory = applicationThreadFactory;
		this.applicationExecutorService = applicationExecutorService;
	}
	
	public ExecutorService createExecutorService() {
		return Executors.newCachedThreadPool(applicationThreadFactory);
	}
	
	public void execute(Runnable command) {
		applicationExecutorService.execute(command);
	}
	
	public <T> Future<T> submit(Callable<T> task) {
		return applicationExecutorService.submit(task);
	}

	public <T> Future<T> submit(Runnable task, T result) {
		return applicationExecutorService.submit(task, result);
	}

	public Future<?> submit(Runnable task) {
		return applicationExecutorService.submit(task);
	}

	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks)
        throws InterruptedException {
		return applicationExecutorService.invokeAll(tasks);
	}

	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks,
                                  long timeout, TimeUnit unit)
        throws InterruptedException {
		return applicationExecutorService.invokeAll(tasks, timeout, unit);
	}


	public <T> T invokeAny(Collection<? extends Callable<T>> tasks)
        throws InterruptedException, ExecutionException {
		return applicationExecutorService.invokeAny(tasks);
	}

   
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks,
                    long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException {
		return applicationExecutorService.invokeAny(tasks, timeout, unit);
	}
}
