package com.guigarage.boxfx.security;

import java.security.Permission;

import com.guigarage.boxfx.concurrency.ApplicationThreadHandler;
import com.guigarage.boxfx.concurrency.ConcurrencyUtilities;

public class ApplicationSecurityManager extends SecurityManager {

	private SecurityManager defaultSecurityManager;
	
	private ApplicationThreadHandler applicationThreadHandler;
	
	public ApplicationSecurityManager(ApplicationThreadHandler applicationThreadHandler) {
		this.applicationThreadHandler = applicationThreadHandler;
		this.defaultSecurityManager = System.getSecurityManager();
	}
	
	public void setAsDefault() {
		System.setSecurityManager(this);
	}
	
	@Override
	public void checkAwtEventQueueAccess() {
		throw new SecurityException("Can't use AWT!");
	}
	
	private boolean isInCheck = false;
	
	@Override
	public void checkAccess(Thread t) {
		if(isInCheck) {
			if(defaultSecurityManager != null) {
				defaultSecurityManager.checkAccess(t);
			}
		} else {
			isInCheck = true;
			if(isInApplication() && applicationThreadHandler.isApplicationThread(t) && !ConcurrencyUtilities.isChildOrEquals(t, applicationThreadHandler.getThreadGroupForApplication(Thread.currentThread()))) {
				isInCheck = false;
				throw new SecurityException("Can't create Thread outside of Application Thread Group!");
			}
			isInCheck = false;
		}
	}
		
	@Override
	public void checkAccess(ThreadGroup g) {
		if(isInCheck) {
			if(defaultSecurityManager != null) {
				defaultSecurityManager.checkAccess(g);
			}
		} else {
			isInCheck = true;
			if(isInApplication() && applicationThreadHandler.isApplicationThreadGroup(g) && !ConcurrencyUtilities.isChildOrEquals(g, applicationThreadHandler.getThreadGroupForApplication(Thread.currentThread()))) {
				isInCheck = false;
				throw new SecurityException("Can't create Thread outside of Application Thread Group!");
			}
			isInCheck = false;
		}
	}
	
	private boolean isInApplication() {
		return applicationThreadHandler.isApplicationThread(Thread.currentThread());
	}
	
	@Override
	public void checkPermission(Permission perm) {
		if(defaultSecurityManager != null) {
			defaultSecurityManager.checkPermission(perm);
		}
	}
	
	@Override
	public void checkPermission(Permission perm, Object context) {
		if(defaultSecurityManager != null) {
			defaultSecurityManager.checkPermission(perm, context);
		}
	}
}
