package com.guigarage.boxfx.container;

import java.io.File;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

import com.guigarage.boxfx.communication.vertx.HttpMethods;
import com.guigarage.boxfx.communication.vertx.VertxHttpService;
import com.guigarage.boxfx.communication.vertx.VertxServer;

public class ApplicationUploadWebService implements VertxHttpService {

	private ApplicationContainer applicationContainer;

	public ApplicationUploadWebService(ApplicationContainer applicationContainer) {
		this.applicationContainer = applicationContainer;
	}

	public void start() {
		VertxServer.getInstance().addHttpService(this);
	}

	@Override
	public Handler<HttpServerRequest> createHandler() {
		return new Handler<HttpServerRequest>() {

			@Override
			public void handle(HttpServerRequest req) {
				if (req.method.trim().toLowerCase().equals(HttpMethods.POST.toString().trim().toLowerCase()) && req.path.trim().toLowerCase().equals("/application-upload")) {
					req.bodyHandler(new Handler<Buffer>() {

						@Override
						public void handle(Buffer data) {
							try {
								byte[] applicationBytes = data.getBytes();
								File tempArchive = Files.createTempFile("box-",
										"-application").toFile();
								FileUtils.writeByteArrayToFile(tempArchive,
										applicationBytes);
								applicationContainer
										.installModuleFromZip(tempArchive
												.getAbsolutePath());
								FileUtils.forceDelete(tempArchive);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}
		};
	}

}
