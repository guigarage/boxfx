package com.guigarage.boxfx.container;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

import org.apache.commons.io.FileUtils;

import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.application.BoxFXApplicationMetadata;
import com.guigarage.boxfx.concurrency.ApplicationThreadHandler;
import com.guigarage.boxfx.container.json.DecodeException;
import com.guigarage.boxfx.container.json.JsonObject;

public class ApplicationContainer {

	private static final int BUFFER_SIZE = 4096;

	private final File containerDirectory;

	private final ClassLoader platformClassLoader;

	private ApplicationUploadWebService applicationUpload;

	private ApplicationThreadHandler applicationThreadHandler;

	private ObservableList<ApplicationMetadata> installedApplications;

	public ApplicationContainer(File containerDirectory,
			ApplicationThreadHandler applicationThreadHandler) {
		this.platformClassLoader = Thread.currentThread()
				.getContextClassLoader();
		this.applicationThreadHandler = applicationThreadHandler;
		this.containerDirectory = containerDirectory;
		this.containerDirectory.mkdirs();

		installedApplications = FXCollections.observableArrayList();

		applicationUpload = new ApplicationUploadWebService(this);
		applicationUpload.start();
	}

	public ObservableList<ApplicationMetadata> getInstalledApplications() {
		return installedApplications;
	}

	public ApplicationThreadHandler getApplicationThreadHandler() {
		return applicationThreadHandler;
	}

	public void installModuleFromZip(final String zipFileName)
			throws IOException, ApplicationContainerException {
		if (zipFileName == null) {
			throw new NullPointerException("zipFileName cannot be null");
		}
		ModuleZipInfo info = new ModuleZipInfo(false, zipFileName);
		File tempDir = Files.createTempDirectory(
				"temp-app" + UUID.randomUUID().toString()).toFile();
		unzipModuleData(tempDir, info, false);
		final ApplicationMetadata applicationMetadata = installModuleFromFileSystem(tempDir);
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				installedApplications.add(applicationMetadata);
			}
		});
	}

	private ApplicationMetadata installModuleFromFileSystem(File currentModDir)
			throws IOException, ApplicationContainerException {
		JsonObject modJSON = loadModuleConfig(currentModDir);
		String groupId = modJSON.getString("groupId");
		String artifactId = modJSON.getString("artifactId");
		String version = modJSON.getString("version");
		String applicationName = BoxFXApplicationMetadata.getUniqueAppName(
				groupId, artifactId, version);
		File appDir = new File(containerDirectory, applicationName);
		if (appDir.exists()) {
			throw new ApplicationContainerException("App bereits vorhanden....");
		}
		appDir.mkdirs();
		FileUtils.copyDirectory(currentModDir, appDir);
		return createApplicationMetadata(applicationName);
	}

	public ApplicationMetadata find(String vendor, String artifactId) {
		for (ApplicationMetadata applicationMetadata : installedApplications) {
			if (applicationMetadata.getVendor() != null
					&& applicationMetadata.getVendor().equals(vendor)
					&& applicationMetadata.getArtifactId() != null
					&& applicationMetadata.getArtifactId().equals(artifactId)) {
				return applicationMetadata;
			}
		}
		return null;
	}

	public synchronized void loadAllApplications() {
		if (containerDirectory.listFiles() != null) {
			for (File appDir : containerDirectory.listFiles()) {
				if (appDir.isDirectory()) {
					try {
						final ApplicationMetadata applicationMetadata = createApplicationMetadata(appDir
								.getName());
						Platform.runLater(new Runnable() {

							@Override
							public void run() {
								installedApplications.add(applicationMetadata);
							}
						});
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public synchronized void uninstall(ApplicationMetadata applicationMetadata)
			throws IOException {
		File appDir = new File(containerDirectory,
				applicationMetadata.getUniqueName());
		if (appDir.exists() && appDir.isDirectory()) {
			FileUtils.deleteDirectory(appDir);
		}
	}

	private ApplicationMetadata createApplicationMetadata(String applicationName)
			throws IOException, ApplicationContainerException {
		File appDir = new File(containerDirectory, applicationName);

		JsonObject modJSON = loadModuleConfig(appDir);

		List<URL> moduleClasspath = getModuleClasspath(appDir);
		ClassLoader applicationClassLoader = new ApplicationClassLoader(
				platformClassLoader,
				moduleClasspath.toArray(new URL[moduleClasspath.size()]));

		BoxFXApplicationMetadata applicationMetadata = new BoxFXApplicationMetadata();
		applicationMetadata.setApplicationClass(modJSON
				.getString("applicationClass"));
		applicationMetadata.setName(modJSON.getString("applicationName"));
		applicationMetadata.setArtifactId(modJSON.getString("artifactId"));
		applicationMetadata.setVendor(modJSON.getString("groupId"));
		applicationMetadata.setVersion(modJSON.getString("version"));

		// TODO: andere Properties setzten...
		applicationMetadata.setIcon(new Image(applicationClassLoader
				.getResourceAsStream(modJSON.getString("applicationIcon"))));
		applicationMetadata.setApplicationClassLoader(applicationClassLoader);
		return applicationMetadata;
	}

	@SuppressWarnings("resource")
	private JsonObject loadModuleConfig(File modDir)
			throws ApplicationContainerException {
		// Checked the byte code produced, .close() is called correctly, so the
		// warning can be suppressed
		try (Scanner scanner = new Scanner(new File(modDir, "app.json"))
				.useDelimiter("\\A")) {
			String conf = scanner.next();
			return new JsonObject(conf);
		} catch (FileNotFoundException e) {
			throw new ApplicationContainerException("Module " + null
					+ " does not contain a mod.json file");
		} catch (NoSuchElementException e) {
			throw new ApplicationContainerException("Module " + null
					+ " contains an empty mod.json file");
		} catch (DecodeException e) {
			throw new ApplicationContainerException("Module " + null
					+ " mod.json contains invalid json");
		}
	}

	private List<URL> getModuleClasspath(File modDir)
			throws ApplicationContainerException {
		List<URL> urls = new ArrayList<>();
		// Add the classpath for this module
		try {
			if (modDir.exists()) {
				urls.add(modDir.toURI().toURL());
				File libDir = new File(modDir, "libs");
				if (libDir.exists()) {
					File[] jars = libDir.listFiles();
					for (File jar : jars) {
						URL jarURL = jar.toURI().toURL();
						urls.add(jarURL);
					}
				}
			}
			return urls;
		} catch (MalformedURLException e) {
			// Won't happen
			throw new ApplicationContainerException(e);
		}
	}

	private String removeTopDir(String entry) {
		int pos = entry.indexOf(System.getProperty("file.separator"));
		if (pos != -1) {
			entry = entry.substring(pos + 1);
		}
		return entry;
	}

	private void unzipModuleData(final File directory,
			final ModuleZipInfo zipinfo, boolean deleteZip)
			throws ApplicationContainerException {
		try (InputStream is = new BufferedInputStream(new FileInputStream(
				zipinfo.filename));
				ZipInputStream zis = new ZipInputStream(
						new BufferedInputStream(is))) {
			ZipEntry entry;
			while ((entry = zis.getNextEntry()) != null) {
				String entryName = zipinfo.oldStyle ? removeTopDir(entry
						.getName()) : entry.getName();
				if (!entryName.isEmpty()) {
					if (entry.isDirectory()) {
						if (!new File(directory, entryName).mkdir()) {
							throw new ApplicationContainerException(
									"Failed to create directory");
						}
					} else {
						int count;
						byte[] buff = new byte[BUFFER_SIZE];
						BufferedOutputStream dest = null;
						try {
							File entryFile = new File(directory, entryName);
							entryFile.getParentFile().mkdirs();
							entryFile.createNewFile();
							OutputStream fos = new FileOutputStream(entryFile);
							dest = new BufferedOutputStream(fos, BUFFER_SIZE);
							while ((count = zis.read(buff, 0, BUFFER_SIZE)) != -1) {
								dest.write(buff, 0, count);
							}
							dest.flush();
						} finally {
							if (dest != null) {
								dest.close();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new ApplicationContainerException("Failed to unzip module", e);
		} finally {
			if (deleteZip) {
				if (!new File(zipinfo.filename).delete()) {
					System.out.println("Failed to delete zip");
				}
			}
		}
	}

	private static final class ModuleZipInfo {
		final boolean oldStyle;
		final String filename;

		private ModuleZipInfo(boolean oldStyle, String filename) {
			this.oldStyle = oldStyle;
			this.filename = filename;
		}
	}
}
