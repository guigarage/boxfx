package com.guigarage.boxfx.container;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

/**
 * Each module (not module instance) is assigned it's own ModuleClassLoader.
 *
 * A ModuleClassLoader can have multiple parents, this always includes the class loader of the module that deployed it
 * (or null if is a top level module), plus the class loaders of any modules that this module includes.
 *
 * This class loader always tries to the load the class itself. If it can't find the class it iterates
 * through its parents trying to load the class. If none of the parents can find it, the platform class loader classloader is tried.
 *
 * When locating resources this class loader always looks for the resources itself, then it asks the parents to look,
 * and finally the platform class loader classloader is asked.
 *
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class ApplicationClassLoader extends URLClassLoader {

  private final ClassLoader platformClassLoader;

  public ApplicationClassLoader(ClassLoader platformClassLoader, URL[] classpath) {
    super(classpath);
    this.platformClassLoader = platformClassLoader;
  }

  @Override
  protected synchronized Class<?> loadClass(String name, boolean resolve)
      throws ClassNotFoundException {

    Class<?> c = doLoadClass(name);
    if (c == null) {
      c = platformClassLoader.loadClass(name);
    }

    if (resolve) {
      resolveClass(c);
    }
    return c;
  }

  protected synchronized Class<?> doLoadClass(String name) {
    Class<?> c = findLoadedClass(name);
    if (c == null) {
      try {
        c = findClass(name);
      } catch (ClassNotFoundException e) {
       //Nix tun, Klasse wird versucht aus Parent-Classlaoder zu laden
      }
    }
    return c;
  }

  @Override
  public synchronized URL getResource(String name) {
    return doGetResource(name, true);
  }

  private URL doGetResource(String name, boolean considerTCCL) {
    try {
      // First try with this class loader
      URL url = findResource(name);
      if (url == null) {

        // Finally try the platform class loader
        url = platformClassLoader.getResource(name);
      }
      return url;
    } finally {
    }
  }


  @Override
  public synchronized Enumeration<URL> getResources(String name) throws IOException {
    final List<URL> totURLs = new ArrayList<>();

    // Local ones
    addURLs(totURLs, findResources(name));

    // And platform class loader too
    addURLs(totURLs, platformClassLoader.getResources(name));

    return new Enumeration<URL>() {
      Iterator<URL> iter = totURLs.iterator();

      public boolean hasMoreElements() {
        return iter.hasNext();
      }

      public URL nextElement() {
        return iter.next();
      }
    };
  }

  @Override
  public InputStream getResourceAsStream(String name) {
    URL url = getResource(name);
    try {
      return url != null ? url.openStream() : null;
    } catch (IOException e) {
    }
    return null;
  }

  private void addURLs(List<URL> urls, Enumeration<URL> toAdd) {
    if (toAdd != null) {
      while (toAdd.hasMoreElements()) {
        urls.add(toAdd.nextElement());
      }
    }
  }


}
