package com.guigarage.boxfx.container;

public class ApplicationContainerException extends Exception {

	private static final long serialVersionUID = 1L;

	public ApplicationContainerException() {
        super();
    }

    public ApplicationContainerException(String message) {
        super(message);
    }

    public ApplicationContainerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationContainerException(Throwable cause) {
        super(cause);
    }
}
