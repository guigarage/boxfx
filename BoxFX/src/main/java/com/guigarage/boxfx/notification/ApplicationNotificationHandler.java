package com.guigarage.boxfx.notification;

import com.guigarage.boxfx.application.ApplicationMetadata;

public interface ApplicationNotificationHandler {

	public void show(ApplicationMetadata applicationMetadata, Notification notification);
}
