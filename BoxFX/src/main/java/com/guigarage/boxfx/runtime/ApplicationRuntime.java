package com.guigarage.boxfx.runtime;

import java.util.concurrent.Callable;

import org.datafx.concurrent.ConcurrentUtils;

import com.guigarage.boxfx.application.Application;
import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.backgroundservice.ApplicationBackgroundRuntime;
import com.guigarage.boxfx.concurrency.ApplicationConcurrencyWrapper;
import com.guigarage.boxfx.concurrency.ApplicationThreadHandler;
import com.guigarage.boxfx.concurrency.BoxApplicationExecutor;
import com.guigarage.boxfx.util.UID;
import com.guigarage.boxfx.view.ApplicationViewWrapper;
import com.guigarage.boxfx.view.BoxView;

public class ApplicationRuntime {

	private Application currentApplication;

	private BoxView boxView;

	private ApplicationThreadHandler applicationThreadHandler;
	
	private ApplicationBackgroundRuntime backgroundRuntime;
	
	public ApplicationRuntime(BoxView boxView, ApplicationThreadHandler applicationThreadHandler, ApplicationBackgroundRuntime backgroundRuntime) {
		this.boxView = boxView;
		this.applicationThreadHandler = applicationThreadHandler;
		this.backgroundRuntime = backgroundRuntime;
	}
	
	public synchronized void closeCurrentApplication()
			throws Exception {
		if (currentApplication != null) {
			final String uid = UID.create();
			ConcurrentUtils.runAndWait(new Runnable() {

				@Override
				public void run() {
					boxView.lockUI(uid);
					currentApplication.prepairStop();
					boxView.removeCurrentApplication(currentApplication
							.getMetadata().getSnapshot());
				}
			});
			
			currentApplication.end();
			
			final ApplicationMetadata lastMetadata = currentApplication.getMetadata();
			applicationThreadHandler.kill(lastMetadata);
			
			applicationThreadHandler.getOrCreateExecutorService(lastMetadata).execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						backgroundRuntime.start(lastMetadata);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			boxView.waitTillAnimationsFinished();

			currentApplication = null;
			
			ConcurrentUtils.runAndWait(new Runnable() {

				@Override
				public void run() {
					boxView.unlockUI(uid);
				}
			});
		}
	}

	public synchronized void startApplication(
			final ApplicationMetadata metadata) throws Exception {
		closeCurrentApplication();
		if (currentApplication != null) {
			throw new Exception("Can't create Application!");
		}
		
		final String uid = UID.create();
		final ApplicationViewWrapper applicationViewWrapper = ConcurrentUtils
				.runAndWait(new Callable<ApplicationViewWrapper>() {

					@Override
					public ApplicationViewWrapper call() throws Exception {
						boxView.lockUI(uid);
						return boxView.showApplication(metadata
								.getSnapshot());
					}
				});

		BoxApplicationExecutor executor = applicationThreadHandler.createApplicationExecutor(metadata);
		final Application application = new ApplicationConcurrencyWrapper(metadata.loadApplication(), executor);
		backgroundRuntime.stop(metadata);
		application.start(metadata);

		ConcurrentUtils.runAndWait(new Runnable() {

			@Override
			public void run() {
				applicationViewWrapper.setApplication(application);
			}
		});
		
		currentApplication = application;

		ConcurrentUtils.runAndWait(new Runnable() {

			@Override
			public void run() {
				boxView.unlockUI(uid);
			}
		});
	}
	
	public Application getCurrentApplication() {
		return currentApplication;
	}
}
