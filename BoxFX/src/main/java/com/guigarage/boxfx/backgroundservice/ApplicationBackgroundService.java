package com.guigarage.boxfx.backgroundservice;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.guigarage.boxfx.application.ApplicationException;
import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.application.task.ApplicationBackgroundTask;
import com.guigarage.boxfx.application.task.ApplicationBackgroundTaskFactory;
import com.guigarage.boxfx.notification.ApplicationNotificationHandler;

public class ApplicationBackgroundService {

	private ApplicationNotificationHandler notificationHandler;

	private ApplicationMetadata applicationMetadata;

	private ExecutorService executorService;

	private List<Future<?>> backgroundTasks;

	public ApplicationBackgroundService(
			ApplicationNotificationHandler notificationHandler,
			ApplicationMetadata applicationMetadata,
			ExecutorService executorService)
			throws InstantiationException, IllegalAccessException,
			ClassNotFoundException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		this.notificationHandler = notificationHandler;
		this.applicationMetadata = applicationMetadata;
		this.executorService = executorService;
		backgroundTasks = new ArrayList<Future<?>>();
	}

	public synchronized void start() throws ApplicationException {
		ApplicationBackgroundTaskFactory backgroundTaskFactory = applicationMetadata
				.loadBackgroundTaskFactory();
		if(backgroundTaskFactory != null) {
			for (final ApplicationBackgroundTask task : backgroundTaskFactory.createTasks()) {
				backgroundTasks.add(executorService.submit(new Runnable() {
	
					@Override
					public void run() {
						boolean interrupted = false;
						while (!interrupted) {
							try {
								task.run();
								Thread.sleep((long) task.getLoopDuration()
										.toMillis());
							} catch (InterruptedException e) {
								interrupted = true;
							}
						}
					}
				}));
			}
		}
	}

	public synchronized void stop() {
		for(Future<?> runningTask : backgroundTasks) {
			runningTask.cancel(true);
		}
		backgroundTasks.clear();
	}

	public ApplicationMetadata getApplicationMetadata() {
		return applicationMetadata;
	}
}
