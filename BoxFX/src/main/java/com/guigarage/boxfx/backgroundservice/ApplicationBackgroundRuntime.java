package com.guigarage.boxfx.backgroundservice;

import java.util.HashMap;
import java.util.Map;

import com.guigarage.boxfx.application.ApplicationException;
import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.concurrency.ApplicationThreadHandler;
import com.guigarage.boxfx.notification.ApplicationNotificationHandler;

public class ApplicationBackgroundRuntime {

	private Map<ApplicationMetadata, ApplicationBackgroundService> backgroundServices;
	
	private ApplicationNotificationHandler notificationHandler;
	
	private ApplicationThreadHandler applicationThreadHandler;
	
	public ApplicationBackgroundRuntime(ApplicationThreadHandler applicationThreadHandler, ApplicationNotificationHandler notificationHandler) {
		backgroundServices = new HashMap<>();
		this.applicationThreadHandler = applicationThreadHandler;
		this.notificationHandler = notificationHandler;
	}
	
	public synchronized void add(ApplicationMetadata applicationMetadata) throws ApplicationException {
		try {
		ApplicationBackgroundService backgroundService = new ApplicationBackgroundService(notificationHandler, applicationMetadata, applicationThreadHandler.createApplicationExecutor(applicationMetadata).createExecutorService());
		backgroundServices.put(applicationMetadata, backgroundService);
		} catch (Exception e) {
			throw new ApplicationException(e);
		}
	}
	
	public synchronized void start(ApplicationMetadata applicationMetadata) throws ApplicationException {
		ApplicationBackgroundService backgroundService = backgroundServices.get(applicationMetadata);
		if(backgroundService != null) {
			backgroundService.start();
		}
	}
	
	public synchronized void stop(ApplicationMetadata applicationMetadata) {
		ApplicationBackgroundService backgroundService = backgroundServices.get(applicationMetadata);
		if(backgroundService != null) {
			backgroundService.stop();
		}
	}
	
	public synchronized void remove(ApplicationMetadata applicationMetadata) {
		stop(applicationMetadata);
		ApplicationBackgroundService backgroundService = backgroundServices.get(applicationMetadata);
		if(backgroundService != null) {
			backgroundServices.remove(applicationMetadata);
		}
	}
}
