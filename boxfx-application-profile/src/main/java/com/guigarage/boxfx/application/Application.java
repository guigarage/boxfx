package com.guigarage.boxfx.application;

import javafx.scene.Node;

public interface Application {

	ApplicationMetadata getMetadata();

	void prepairStop();
	
	void start(ApplicationMetadata applicationMetadata);
	
	void showMenu();
	
	void pause();
	
	void resume();
	
	Node show();
	
	void end();
}
