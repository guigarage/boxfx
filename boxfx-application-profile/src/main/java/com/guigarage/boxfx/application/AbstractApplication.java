package com.guigarage.boxfx.application;



public abstract class AbstractApplication implements Application {

	private ApplicationMetadata applicationMetadata;
	
	@Override
	public ApplicationMetadata getMetadata() {
		return applicationMetadata;
	}
	
	@Override
	public void start(ApplicationMetadata applicationMetadata) {
		this.applicationMetadata = applicationMetadata;
	}
	
	@Override
	public void showMenu() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}
	
	@Override
	public void end() {}
	
	@Override
	public void prepairStop() {}
}
