package com.guigarage.boxfx.application;

import com.guigarage.boxfx.application.task.ApplicationBackgroundTaskFactory;

import javafx.scene.image.Image;

public interface ApplicationMetadata {
	
	public String getName();	
	
	public String getVendor();
	
	public String getVersion();
	
	public String getApplicationClass();
	
	public String getBackgroundTaskFactoryClass();
	
	public Image getSnapshot();
	
	public Image getIcon();
	
	public String getArtifactId();
	
	public ClassLoader getApplicationClassLoader();
	
	public <T extends Application> T loadApplication() throws ApplicationException;

	public ApplicationBackgroundTaskFactory loadBackgroundTaskFactory() throws ApplicationException;
	
	public String getUniqueName();
}
