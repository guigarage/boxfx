package com.guigarage.boxfx.notification;

public interface NotificationHandler {

	void show(Notification notification);
}
